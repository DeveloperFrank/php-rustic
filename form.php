<!DOCTYPE html>
<html lang="en">
<?php require("index.php"); ?>
<head>
	<meta charset="UTF-8">
	<title>formulario</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h2>Formulario de registro</h2>
		</div>
		<div class="form" style="width: 250px">
			<div class="row">
			<form action="data.php" method="POST">
				<div class="row">
					<div class="col-xs-1">
						<label for="">Nombre</label>
					</div>
					<div class="col-xs-11">
						<input type="text" name="nombre" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-1">
						<label for="">Apellido</label>
					</div>
					<div class="col-xs-11">
						<input type="text" name="apellido" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-1">
						<label for="">email</label>
					</div>
					<div class="col-xs-11">
						<input type="text" name="email" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-1">
						<label for="">Telefono</label>
					</div>
					<div class="col-xs-11">
						<input type="text" name="telefono" class="form-control">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-1">
						<label for="">Ciudad: </label>
					</div>
					<div class="col-xs-11">
						<select name="ciudad" class="form-control">
							<?php while ($row = mysql_fetch_array($ciudades)) { ?>
								<option value="<?php echo $row['id'] ?>"><?php echo $row['nombre'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<br>
					<input type="submit" value="Enviar">
			</form>
		</div>
		<hr>
		</div>
		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th><strong>ID</strong></th>
						<th><strong>Nombre</strong></th>
						<th><strong>Apellido</strong></th>
						<th><strong>Email</strong></th>
						<th><strong>Telefono</strong></th>
						<th><strong>Ciudad</strong></th>
					</tr>
				</thead>
				<tbody>
					<?php while($row_clientes = mysql_fetch_array($clientes)){ ?>
						<tr>
							<td><?php echo $row_clientes['id'] ?></td>
							<td><?php echo $row_clientes['nombre'] ?></td>
							<td><?php echo $row_clientes['apellido'] ?></td>
							<td><?php echo $row_clientes['email'] ?></td>
							<td><?php echo $row_clientes['telefono'] ?></td>
							<?php if ($row_clientes['id_ciudad'] == 5) { ?>
								<td><font color="red"><?php echo $row_clientes['descripcion'] ?></font></td>
							<?php }else{ ?>
								<td><?php echo $row_clientes['descripcion'] ?></td>
							<?php } ?>
						</tr>
					<?php } ?>					
				</tbody>

			</table>
			
		</div>
	</div>
</body>
</html>